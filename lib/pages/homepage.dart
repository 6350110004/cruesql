import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test1234/pages/about.dart';
import 'package:test1234/pages/details.dart';
import 'package:test1234/pages/insert.dart';

import '../models/products.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return productsFromJson(response.body);
    } else {
      throw Exception('Failed to load Products');
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.add_box),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Create()),
              );
            },
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Products>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Card(
                    color: Colors.white12,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 10,
                    child: Container(
                      child: Center(
                        child: ListTile(
                          leading: Icon(Icons.playlist_add),
                          title: Text(
                            data.name,
                            style: TextStyle(fontSize: 20),
                          ),
                          subtitle: Text(data.price),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Details(product: data)),
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(243, 229, 108, 1.0),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
            children: <Widget>[
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'HOME',
                icon: Icons.home,
                onClicked: () => selectedItem(context, 0),
              ),
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'ABOUT',
                icon: Icons.people_alt,
                onClicked: () => selectedItem(context, 1),
              ),
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'CLOSE',
                icon: Icons.close_sharp,
                onClicked: () => selectedItem(context, 0),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.black;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;

      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const About()),
        );
        break;
    }
  }
}
